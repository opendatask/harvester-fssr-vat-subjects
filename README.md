# Introduction

[OpenData.sk initiative](http://opendata.sk) is, as part of our activities, among other things trying to help with publishing and usage of Open Data.

The purpose of this harvester is to:

1. help maintain copies of VAT related information from
   [Financial Administration of Slovak Republic](https://www.financnasprava.sk/sk/elektronicke-sluzby/verejne-sluzby/zoznamy/exporty-z-online-informacnych)
    - mainly by avoiding ZIP, see [rationale from Open Data CR](https://opendata.gov.cz/%C5%A1patn%C3%A1-praxe:komprese) (in Czech)
2. facilitate creation of other services based on that data

# License

EUPL 1.2, see [LICENSE](LICENSE) for more details.

## 3rd party components 

- bash
- curl
- git
- gpg2
- unzip
