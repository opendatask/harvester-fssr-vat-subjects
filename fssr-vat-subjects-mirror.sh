#!/bin/bash
#
# Copyright (C) 2022 Peter Hanecak <hanecak@opendata.sk>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence. You may
# obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

# defaults
SYSTEM_CONFIG=/etc/sysconfig/fssr-vat-subjects-mirror
USER_CONFIG=~/.fssr-vat-subjects-mirror
DATA_GIT_REPO=
DATA_GIT_PUSH=0
SIGN_KEY=

# load config
[[ -f "${SYSTEM_CONFIG}" ]] && source $SYSTEM_CONFIG
[[ -f "${USER_CONFIG}" ]] && source $USER_CONFIG

# basic checks
[[ -z "${DATA_GIT_REPO}" ]] && echo "ERROR: DATA_GIT_REPO not set, please set it in $SYSTEM_CONFIG or $USER_CONFIG" && exit -1
[[ ! -d "${DATA_GIT_REPO}" ]] && echo "ERROR: data git directory dues not exist: $DATA_GIT_REPO" && exit -1

# stuff for signing of a file:
#	$1 file to sign
function sign_file() {
	[[ -f "${1}.sig" ]] && rm "${1}.sig"
	gpg2 --detach-sign --batch \
		--default-key "${SIGN_KEY}" \
		"${1}"
}

# stuff for mirroring of one data set:
#	$1 - alias/dataset name
#	$2 - source URL
function mirror() {
	data_dir="${DATA_GIT_REPO}/${1}"
	headers_file="${data_dir}/${1}-headers.txt"
	etag_file_new="${data_dir}/${1}-etag.txt.tmp"
	etag_file_old="${data_dir}/${1}-etag.txt"
	data_zip_file="${data_dir}/${1}.zip"

	[[ ! -d "${data_dir}" ]] && mkdir "${data_dir}"
	[[ ! -f "${etag_file_old}" ]] && touch "${etag_file_old}"

	# download
	errored=0
	curl \
		-f --no-progress-meter \
		-D "${headers_file}" \
		--etag-save "${etag_file_new}" \
		--etag-compare "${etag_file_old}" \
		-o "${data_zip_file}" \
		"$2"
	[ $? != 0 ] && echo "ERROR: download of $2 failed" && errored=1
	echo "info: download complete: $2"

	# determine whether changes occurred in data
	etag_new=$(<${etag_file_new})
	etag_old=$(<${etag_file_old})
	changed=0
	[[ "${etag_new}" != "${etag_old}" && $errored == 0 ]] && changed=1

	# handle etags depending on success or error
	if [[ $errored == 0 ]]; then
		mv "${etag_file_new}" "${etag_file_old}"
	else
		rm -f "${etag_file_new}"
	fi

	# unpack
	if [[ "${changed}" == 1 ]]; then
		unzip -d "${data_dir}" -o "${data_zip_file}"
	else
		echo "info: unzip skipped since no changes detected"
	fi

	# signing
	if [[ ! -z "${SIGN_KEY}" ]]; then
		# always sign header file, since it contains date of this operation
		sign_file "${headers_file}"
		# the rest only when change occurred
		if [[ "${changed}" == 1 ]]; then
			for sign_file in "${data_dir}"/ds_*.x?? "${etag_file_old}" "${data_zip_file}"; do
				sign_file "${sign_file}"
		done
		fi
	fi

	# commit to data git repo and push
	date_now=$(date --iso-8601=seconds)
	date_now_utc=$(date --iso-8601=seconds -u)
	git_tag=$(date +%Y%m%d-%H%M)-${1}

	git -C "${DATA_GIT_REPO}" \
		add \
		"${data_zip_file}" \
		"${data_dir}"/ds_*.x?? \
		"${etag_file_old}" \
		"${headers_file}"
	if [[ ! -z "${SIGN_KEY}" ]]; then
		git -C "${DATA_GIT_REPO}" \
			add \
			"${data_dir}"/*.sig
	fi
	git -C "${DATA_GIT_REPO}" \
		commit -m "automatic update: $1, ${date_now_utc} (local: ${date_now})"
	git -C "${DATA_GIT_REPO}" \
		tag -f "${git_tag}"
	if [[ "$DATA_GIT_PUSH" == 1 ]]; then
		git -C "${DATA_GIT_REPO}" push --all
		git -C "${DATA_GIT_REPO}" push --tags
	fi
}

# stuff for handling of multiple datasets
mirror ds_dphs		https://report.financnasprava.sk/ds_dphs.zip
mirror ds_dph_iban	https://report.financnasprava.sk/ds_dph_iban.zip
mirror ds_dph_oud	https://report.financnasprava.sk/ds_dph_oud.zip
